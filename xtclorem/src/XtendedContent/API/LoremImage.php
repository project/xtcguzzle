<?php


namespace Drupal\xtclorem\XtendedContent\API;


use Drupal\Core\Url;

class LoremImage {

  /**
   * @param string $text
   * @param int $width
   * @param int $height
   * @param string $background_color
   * @param string $text_color
   * @param string $format
   *
   * @return string
   */
  public static function getTextImage(
    $text = '',
    int $width = 600,
    int $height = 400,
    $background_color = '000',
    $text_color = 'fff',
    $format = 'png'
  ) {
//    $text = 'Ceci est mon texte';
//    dump(LoremImage::getTextImage($text,300, 200));

    $uri = [
      'service' => 'https://dummyimage.com',
      'size' => $width . 'x' . $height,
      'background_color' => $background_color,
      'text_color' => $text_color,
    ];
    $path = implode('/', $uri) . '.' . $format . '?text=' . $text;
    return $path;
  }

}
