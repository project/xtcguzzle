<?php

namespace Drupal\xtcguzzle\Plugin\XtcHandler;


use Drupal\Component\Serialization\Json;
use Drupal\Core\Site\Settings;
use Drupal\key\Entity\Key;
use Drupal\xtc\PluginManager\XtcHandler\XtcHandlerPluginBase;
use Drupal\xtc\XtendedContent\API\XtcLoaderServer;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Uri;

/**
 * Plugin implementation of the xtc_handler for File.
 *
 */
abstract class GuzzleBase extends XtcHandlerPluginBase {

  /**
   * @var string
   */
  protected $url;

  /**
   * @var Uri
   */
  protected $uri;

  /**
   * @var Client
   */
  protected $client;

  public function values() {
    return Json::decode($this->content) ?? NULL;
  }

  /**
   * @param array $options
   *
   * @return \Drupal\xtc\PluginManager\XtcHandler\XtcHandlerPluginBase
   */
  public function setOptions($options = []): XtcHandlerPluginBase {
    parent::setOptions($options);
    $this->setUrl();
    $this->options['base_uri'] = $this->getUrl();
    $this->setCookie();
    $this->setToken();
    return $this;
  }

  /**
   * @return string
   */
  public function getUrl() {
    return $this->url;
  }

  public function setUrl(): GuzzleBase {
    if($this->profile['server'] == 'fullpath' && !empty($this->options['path'])){
      $this->url = $this->options['path'];
      return $this;
    }

    $settings = Settings::get('xtc_servers');
    $server = XtcLoaderServer::load($this->profile['server']);
    $current_env = $settings[$this->profile['server']]['env'] ?? $server['env'];
    $env = $server['connection'][$current_env];

    if ('self' == $env['host']) {
      $requestServer = \Drupal::request()->server;
      $protocole = ($requestServer->get('HTTP_X_FORWARDED_PROTO')) ? 'https' : 'http';
      $domain = ($requestServer->get('HTTP_HOST')) ?? '';
      $port = ($requestServer->get('HTTP_X_FORWARDED_PORT')) ? ':' . $requestServer->get('HTTP_X_FORWARDED_PORT') : '';
    }
    else {
      $protocole = ($env['tls']) ? 'https' : 'http';
      $domain = $env['host'];
      $port = (!empty($env['port'])) ? ':' . $env['port'] : '';
    }
    $base_url = $protocole . '://' . $domain . $port;

    if (!empty($this->options['query'])) {
      $this->profile['query'] = $this->options['query'];
    }

    if (empty($this->method) && !empty($this->options['method'])) {
      $this->method = $this->options['method'];
    }
    if (empty($this->method) && !empty($this->profile['method'])) {
      $this->method = $this->profile['method'];
    }
    if (!empty($this->profile['query'])) {
      $this->method = $this->method . '/' . $this->profile['query'];
    }
    $this->endpoint = $env['endpoint'];
    $this->uri =
      (!empty($this->endpoint)) ? $this->endpoint . '/' . $this->method
        : $this->method;
    $this->url = $base_url . '/' . $this->uri;
    return $this;
  }

  public function setCookie() {
    $requestServer = \Drupal::request()->server;
    $cookie = $requestServer->get('HTTP_COOKIE');
    if (!empty($cookie)) {
      $this->options['headers']['Cookie'] = $cookie;
    }
  }

  public function setToken() {
    $token = $this->profile['token'] ?? [];
    $key = '';
    if (!empty($token) && !empty($token['type'])) {
      if(!empty($token['src']) && !empty($token['name'])) {
        switch ($token['src']) {
          case 'key':
            $keyEntity = Key::load($token['name']);
            $key = $keyEntity->getKeyValue();
            break;
          default:
        }
      } else {
        $key = $token['value'] ?? '';
      }
      switch ($token['type']) {
        case 'bearer':
          //          $this->options['headers']['Authorization'] = 'Bearer ' . $token['value'];
          $this->options['headers']['Authorization'] = 'Bearer ' . $key;
          break;
        default:
      }
    }
  }

  /**
   * @return Uri
   */
  public function getUri() {
    return $this->uri;
  }

  protected function initProcess() {
    $this->buildClient();
  }

  protected function runProcess() {
    $this->getStream();
  }

  protected function adaptContent() {
  }

  protected function buildClient(): GuzzleBase {
    // Conflict name between XTC Handler and Guzzle Client Handler.
    if(!empty($this->options['handler'])) {
      unset($this->options['handler']);
    }
    $this->client = New Client($this->options);
    return $this;
  }

  protected function getStream() {
    $arrDefaultQs = [];
    $arrUrlQs = [];

    $qs = $this->options['qs'] ?? '';
    parse_str($qs, $arrDefaultQs);
    if(empty($this->options['no-override'])){
      $qs = \Drupal::request()->getQueryString() ?? $qs;
      parse_str($qs, $arrUrlQs);
    }
    $newQs = array_merge($arrDefaultQs, $arrUrlQs);
    $qs = http_build_query($newQs);

    $request = (!empty($qs)) ? $this->url . '?' . $qs : $this->url;
    try {
      $stream = $this->treatStream($request);
      $this->content = $stream->getBody()->getContents();
    } catch (\Exception $exception) {
      $this->content = Json::encode(['Exception' => $exception->getMessage()]);
    }
  }

  protected function treatStream($request) {
    return $this->client->get($request);
  }

  /**
   * @return string[]
   */
  public static function getStruct(): array {
    return ['server', 'method', 'query', 'token'];
  }

}
