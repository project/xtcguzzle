<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcguzzle\Plugin\XtcHandler;


/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "guzzle_get",
 *   label = @Translation("Guzzle Get for XTC"),
 *   description = @Translation("Guzzle Get for XTC description.")
 * )
 */
class GuzzleGet extends GuzzleBase {

}
