<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcguzzle\Plugin\XtcHandler;


use Drupal\Component\Serialization\Json;

/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "guzzle_post",
 *   label = @Translation("Guzzle Post for XTC"),
 *   description = @Translation("Guzzle Get for XTC description.")
 * )
 */
class GuzzlePost extends GuzzleBase {

  protected function treatStream($request) {
    return $this->client->post($request);
  }

}
